docker-machine version


## start container
```
docker run -p 80:80 nginx
docker run -p 3000:80 nginx     <out:in>
docker run -d --name webhost -p 80:80 nginx
```

## view logs
```
docker logs webhost
```

## view process
```
docker top webhost
docker stats webhost
```

## Shell
new container
```
docker run -it --name webhost nginx sh
```

existing container
```
docker exec -it webhost sh
```

## Networking
docker port webhost

use custom virtual networks for connect apps instead use the deault `bridge` network.

show networks
docker network ls

create network
docker network create my_net
docker run -d --name webhost --network my_net nginx

inspect network
docker network inspect my_net


# IMAGES
show the information of the image, ports
docker inspect nginx

docker image tag <source> <target>:<tag>


## Dockerfile
```
FROM debian:jessie
ENV VERSION 1.11
RUN apt-get install nginx
# forward response and error logs to docker collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/ngix/error.log
EXPOSE 80 443
CMD ["nginx", "-g"]
```

Build
```
docker build -t custom_nginx .
```



Example
```
FROM nginx
WORKDIR /usr/share/nginx/html
COPY index.html index.html

# from contains the CMD command
```




prune
```
docker image prune
docker system prune
docker image prune -a
docker system df   # usage space
```


## Volumes

volume created by default in the Dockerfile 
```
FROM
RUN
VOLUME /var/lib/mysq
CMD
```
This volume will be mount into the container, and it will be kep in the host.

```
docker volume ls
docker volume inspect <volume_name>
```
Name the volumes when a container is being executed. To make easy to track and delete the related volume.

## Bind mount
mount a file into a container. this file can be modified from the container.
```
docker run -d --name nginx -p 80:80 -v $(pwd):/usr/share/nginx/html nginx
```



# DOCKER-COMPOSE

```
version: "3.1"
    service: # containers, same as docker run
        servicename: # friendly name. DNS name inside the network
            image: # for build
            command: # replace the default CMD of the image
            environment: # -e of docker run
            volumes: -v o docker run
        servicename2:
    volumes: # same as docker volume create
    networks: # same as docker network create
```




# SWARM

```
docker info
docker swarm init
```

locale error:
```
sudo locale-gen "en_US.UTF-8"
```


docker service replace docker run
```
docker service --help
```

create a service
```
docker service create alpine ping 8.8.8.8
```

see service running
```
docker service ls
```

see containers
```
docker service ps vibrant_booth
docker ps -a
```


increase replicas
```
docker service update l1z2hc88qjt1 --replicas 3
```


a container can be killed and swarm will create a new one.



## 3-node swarm cluster


Create nodes, requires virtualbox
```
docker-machine create node1
docker-machine ssh node1
```

Inside a node

```
docker swarm init --advertise-addr <public ip of the node>
```

in the other nodes paste the command generated on node1
```
docker service create --replicas 3 alpine ping 8.8.8.8
docker node ps
```
the replicas will spread across the nodes


### Create a service inside the cluster
```
docker network create --driver overlay mydrupal
docker network ls
docker service create --name psql --network mydrupal -e POSTGRES_PASSWORD=pass postgres
docker service ls
docker service ps psql
docker container logs psql-xx-node-container-id
docker service create --name drupal --network mydrupal -p 80:80 drupal
# copy dns from service name > psql
docker service logs worke    (name)
```

watch how the services start...
```
watch docker service ls
```
the swarm cluster already have a load balancer to redirect trafic to the services




# Swarm stacks

Use docker-compose files to deploy services
```
docker stack deploy
```

```
version: "3"
services:
    redis:
        image: redis-alpine
        ports:
            - 6379
        networks:
            - frontend
        deploy:
            replicas: 2
            update_config:
                parallelism: 2
                delay: 10s
            restart_policy:
                condition: on-failure
    db:
        image: postgres:9.4
        volumes:
            - db-data:/var/lib/postgresql/data
        networks:
            - backend
        deploy:
            placement:
                constrains: [node.role == manager]
    vote:
        image: dockersamples/examplevotingapp_vote:before
        ports:
            - 5000:80
        network:
            - frontend
        depends_on:
            - redis
        deploy:
            replicas: 2
            update_config:
                parallelism: 2
            restart_policy:
                condition: on-failure
    result:
        image: dockersamples/examplevotingapp_result:before
        ports:
            - 5001:80
        networks:
            - backend
        depends_on:
            - db
        deploy:
            replicas: 1
            update_config:
                parallelism: 2
                delay: 10s
            restart_policy:
                condition: on-failure
    worker:
        image: dockersamples/examplevotingapp_worker
        network:
            - backend
            - frontend
        deploy:
            mode: replicated
            replicas: 1
            labels: [APP=VOTING]
            restart_policy:
                condition: on-failure
                delay: 10s
                max_attempts: 3
                window: 120s
            placement:
                constrains: [node.role == manager]
```

run the services
```
docker stack deploy -c example-voting-app-stack.yml voteapp
```

show services info
```
docker stack --help
docker stack ps voteapp
docker container ls
docker stack services voteapp
docker stack ps voteapp
docker network ls
```
additionally an additional service is created - visualizer. Is a web service running in the port :8080 that shows the server memory and resources.

update the replicas and run again the deployment 
```
docker stack deploy -c example-voting-app-stack.yml voteapp
```



### Secrets for stacks


Secrets for services
```
# file created psql_user.txt
docker secret create psql_user psql_user.txt
echo "mydbpass" | docker secret create psql_pass -   # - means stdout of echo
docker secret ls

docker service create --name psql --secret psql_user --secret psql_pass -e POSTGRES_PASSWORD_FILE=/run/secrets/psql_pass -e POSTGRES_USER_FILE=/run/secrets/psql_user postgres
```

secrets for stacks

create a service first, and some files for the secrets
```
docker service create --name search --replicas 3 -p 9200:9200 elasticsearch:2
```

then manage the secrets
```
version: "3"
services:
    psql:
        image: postgres:9.4
        secrets:
            - psql_user
            - psql_pass
        environment:
            - POSTGRES_PASSWORD_FILE=/run/secrets/psql_pass
            - POSTGRES_USER_FILE=/run/secrets/psql_user postgres
secrets:
    psql_user:
        file: ./psql_user.txt
    psql_pass:
        file: ./psql_pass.txt
```



```
docker stack deploy -c docker-compose.yml mydb
docker stack rm mydb
```




## Update services
- Image updated to a new version
```
docker service update --image myapp:1.2.1 <servicename>
```
- Add an env var and remove a port
```
service update --env-add NODE_ENV=prod --publish-rm 8080
```
- Change number of replicas of two services
```
docker service scale web=6 api=6
```
- change port
```
docker service update --publis-rm 8088 --publis-add 8080:80
```



## Update services using docker-compose - Swarm
```
docker stack deploy -c file.yml <stackname>
```


## Docker healthcheck
```
docker run \
--name web -d
--health-cmd="curl -f localhost:9200/_cluster/health || fasle" \
--health-interval=5s \
--health-retries=3 \
--health-timeout=2s \
--health-start-period=15s \
elasticsearch:2
```

dockerfile
```
FROM nginx

HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost/ || exit 1
```

```
FROM postgres

HEALTHCEHCK --interval=5s --timeout=3s CMD pg_ready -U postgres || exit 1
```

composer
```
version: "3"
services:
    web:
        image: nginx
        healthcheck:
            test: ["CMD", "curl", "-f", "http://localhost"]
            interval: 1m30s
            timeout: 10s
            retries: 10s
            start_period: 1m
```





































